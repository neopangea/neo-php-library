<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/27/14
 * Time: 2:59 PM
 */

namespace Neo\Lib\DataTypes;


abstract class EnumBase {

    public $id;
    public $key;
    public $label;

    static protected $ids_instance    = array();
    static protected $labels_instance = array();
    static protected $keys_instance   = array();

    public function __construct( $id, $key, $label = null ) {

        $this->id    = $id;
        $this->key   = $key;
        $this->label = is_null($label) ? $key : $label;

        if( array_key_exists( $id, static::$ids_instance )){
            throw new Exception( "Id '{$id}' already exists in this enum ids instance array." );
        }

        if( array_key_exists( $key, static::$keys_instance )){
            throw new Exception( "Key '{$key}' already exists in this enum key instance array." );
        }


        static::$ids_instance[$this->id]       = $this;
        static::$labels_instance[$this->label] = $this;
        static::$keys_instance[$this->key]     = $this;
    }

    static public function get( $ids ){

        // Param $ids may be a single id or and array of ids

        if( is_array( $ids ) ){
            $enums = array();
            foreach( $ids as $id ){
                if( array_key_exists( $id, static::$ids_instance )){
                    array_push( $enums, static::$ids_instance[ $id ] );
                }
            }
            return $enums;
        }

        // Not an array, must be a single id.

        $id = $ids;
        if( ! array_key_exists( $id, static::$ids_instance )){
            return null;
        }
        return static::$ids_instance[ $id ];
    }

    static public function getByKeys( $keys ){

        // Param $ids may be a single id or and array of ids

        if( is_array( $keys ) ){
            $enums = array();
            foreach( $keys as $key ){
                if( array_key_exists( $key, static::$keys_instance )){
                    array_push( $enums, static::$keys_instance[ $key ] );
                }
            }
            return $enums;
        }

        // Not an array, must be a single id.

        $key = $keys;
        if( ! array_key_exists( $key, static::$keys_instance )){
            return null;
        }
        return static::$keys_instance[ $key ];
    }

    static public function getAll(){
        return array_values( static::$ids_instance );
    }

    static public function hasId( $id ){
        return array_key_exists( $id, static::$ids_instance );
    }

    static public function toIds( $arr_enums ){
        $ids = array();
        foreach( $arr_enums as $enum ){
            array_push( $ids, $enum->id );
        }
        return $ids;
    }
}