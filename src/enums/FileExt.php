<?php

/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/13/14
 * Time: 4:01 PM
 */

namespace Neo\Lib\Enums;


class FileExt extends \Neo\Lib\DataTypes\EnumBase {

    const
        NEO_FILE_TYPE_IMAGE    = 'image',
        NEO_FILE_TYPE_AUDIO    = 'audio',
        NEO_FILE_TYPE_VIDEO    = 'video',
        NEO_FILE_TYPE_DOCUMENT = 'document',
        NEO_FILE_TYPE_MISC     = 'misc';

    public $type;

    static protected $ids_instance      = array();
    static protected $labels_instance   = array();
    static protected $images_instance   = array();
    static protected $audio_instance    = array();
    static protected $document_instance = array();
    static protected $video_instance    = array();

    // Images
    public static $Gif;
    public static $Jpeg;
    public static $Jpg;
    public static $Png;
    public static $Svg;

    // Audio
    public static $Mp3;
    public static $Ogg;
    public static $Wav;

    // Document
    public static $Pdf;

    // Video
    public static $Mov;
    public static $Avi;
    public static $Mp4;


    public function __construct( $id, $key, $label, $type) {
        parent::__construct( $id, $key, $label );

        $this->type = $type;

        switch ($this->type) {
            case static::NEO_FILE_TYPE_IMAGE:
                array_push( self::$images_instance, $this );
                break;
            case static::NEO_FILE_TYPE_AUDIO:
                array_push( self::$audio_instance, $this );
                break;
            case static::NEO_FILE_TYPE_DOCUMENT:
                array_push( self::$document_instance, $this );
                break;
            case static::NEO_FILE_TYPE_VIDEO:
                array_push( self::$video_instance, $this );
                break;
        }
    }

    public function getImageExts () {
        return self::$images_instance;
    }

    public function getAudioExts () {
        return self::$audio_instance;
    }

    public function getDocumentExts () {
        return self::$document_instance;
    }

    public function getVideoExts () {
        return self::$document_instance;
    }

    public static function getAllFileTypes () {
        $reflection = new \ReflectionClass('\Neo\Lib\Enums\FileExt');
        $constants  = $reflection->getConstants();
        $file_types = array();
        foreach ($constants as $key=>$val) {
            if (strpos($key, 'NEO_FILE_TYPE') !== false) {
                array_push($file_types, $val);
            }
        }
        return $file_types;
    }
}

FileExt::$Gif   = new FileExt( 1, 'gif'  , 'gif'  , FileExt::NEO_FILE_TYPE_IMAGE    );
FileExt::$Jpeg  = new FileExt( 2, 'jpeg' , 'jpeg' , FileExt::NEO_FILE_TYPE_IMAGE    );
FileExt::$Jpg   = new FileExt( 3, 'jpg'  , 'jpg'  , FileExt::NEO_FILE_TYPE_IMAGE    );
FileExt::$Png   = new FileExt( 4, 'png'  , 'png'  , FileExt::NEO_FILE_TYPE_IMAGE    );
FileExt::$Mp3   = new FileExt( 5, 'mp3'  , 'mp3'  , FileExt::NEO_FILE_TYPE_AUDIO    );
FileExt::$Pdf   = new FileExt( 6, 'pdf'  , 'pdf'  , FileExt::NEO_FILE_TYPE_DOCUMENT );
FileExt::$Mov   = new FileExt( 7, 'mov'  , 'mov'  , FileExt::NEO_FILE_TYPE_VIDEO    );
FileExt::$Avi   = new FileExt( 8, 'avi'  , 'avi'  , FileExt::NEO_FILE_TYPE_VIDEO    );
FileExt::$Ogg   = new FileExt( 9, 'ogg'  , 'ogg'  , FileExt::NEO_FILE_TYPE_AUDIO    );
FileExt::$Wav   = new FileExt(10, 'wav'  , 'wav'  , FileExt::NEO_FILE_TYPE_AUDIO    );
FileExt::$Mp4   = new FileExt(11, 'mp4'  , 'mp4'  , FileExt::NEO_FILE_TYPE_VIDEO    );
FileExt::$Svg   = new FileExt(12, 'svg'  , 'svg'  , FileExt::NEO_FILE_TYPE_IMAGE    );