<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/25/14
 * Time: 10:18 AM
 */

namespace Neo\Lib\Utils;


class String {
    /**
     * Gets first letter of string.
     *
     * @param  string $str
     * @return string Returns first letter of $str. Blank if '' or null
     */

    static public function getFirstLetter ($str) {
        return substr($str, 0, 1 );
    }

    static public function areSameWord ($str1, $str2) {
        return trim(strtolower($str1)) === trim(strtolower($str2));
    }

    static public function isNullOrEmpty ($str) {
        if (is_null($str)) {return true;}
        if (trim($str) == '') {return true;}
        return false;
    }
} 