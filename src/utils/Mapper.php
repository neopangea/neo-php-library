<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/27/14
 * Time: 11:14 AM
 */

namespace Neo\Lib\Utils;


class Mapper {

    const
        TYPE_STRING             = 'string',
        TYPE_STRING_TO_LOWER    = 'stringToLower',
        TYPE_INT                = 'int',
        TYPE_MONGO_ID           = 'MongoId',
        TYPE_MONGO_ID_AS_STRING = 'MongoIdAsString',
        TYPE_DO_NOT_CAST        = 'DoNotCast';


    public static function mapValue (&$target, $source, $name, $type, $options=array()) {

        $source = (object)$source;

        if(isset($source->$name)) {

            $value = null;
            switch ($type) {
                case static::TYPE_STRING:
                    $value = (string) $source->$name;
                    break;
                case static::TYPE_STRING_TO_LOWER:
                    $value = strtolower((string) $source->$name);
                    break;
                case static::TYPE_INT:
                    $value = (int) $source->$name;
                    break;
                case static::TYPE_MONGO_ID:
                    $value = new \MongoId($source->$name);
                    break;
                case static::TYPE_MONGO_ID_AS_STRING:
                    $value = $source->$name->{'$id'};
                    break;
                default:
                    $value = $source->$name;
            }

            if (is_array($target)) {
                $target[$name] = $value;
            } else {
                $target->$name = $value;
            }
        } else if (isset($options['default'])) {
            if (is_array($target)) {
                $target[$name] = $options['default'];
            } else {
                $target->$name = $options['default'];
            }
        }
    }

    public static function mapValues (&$target, $source, $fields, $options=array()) {

        foreach ($fields as $name=>$type) {
            Mapper::mapValue($target, $source, $name, $type, $options);
        }
    }
} 