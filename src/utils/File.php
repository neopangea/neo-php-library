<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/25/14
 * Time: 10:18 AM
 */

namespace Neo\Lib\Utils;

use Neo\Lib\Enums as Enums;
use Neo\Lib\Utils as Utils;

class File {

    /**
     * Trims trailing back or forward slash.
     *
     * @param $filename
     * @return string
     */

    static function trimRightSlash ($filename) {
        $filename = trim($filename);
        $filename = rtrim($filename, '/');
        $filename = rtrim($filename, '\\');
        return $filename;
    }

    /**
     * Trims beginning back or forward slash.
     *
     * @param $filename
     * @return string
     */

    static function trimLeftSlash ($filename) {
        $filename = trim($filename);
        $filename = ltrim($filename, '/');
        $filename = ltrim($filename, '\\');
        return $filename;
    }

    /**
     * Returns a file's ext
     *
     * @param $filename
     * @return mixed
     */

    static function getFileExt ($filename) {
        $sections = explode( '.', $filename );
        return end( $sections );
    }

    /**
     * Removes file ext from filename
     *
     * @param $filename
     * @return mixed
     */

    static public function removeFileExt ($filename) {
        return str_replace( '.' . self::getFileExt( $filename ), '', $filename );
    }

    /**
     * Add postfix to a file's name Ex: hello.gif -> hello_orig.gif
     *
     * @param $filename
     * @param $postfix
     * @return string
     */
    static function addPostfix( $filename, $postfix ) {
        return self::removeFileExt( $filename ) . $postfix . '.' . self::getFileExt( $filename );
    }

    /**
     * Returns a pathed file's file name
     *
     * @param $pathed_filename
     * @return mixed
     */

    static function getFilename( $pathed_filename ){
        $pathed_filename = str_replace( '\\', '/', $pathed_filename );
        $sections = explode( '/', $pathed_filename );
        return end( $sections );
    }

    /**
     * Returns FileExt enum const for file type
     * @param $filename
     * @return string
     */

    static function getFileType ($filename) {
        $ext = static::getFileExt($filename);
        $known_exts = Enums\FileExt::getAll();

        foreach ($known_exts as $known_ext) {
            if (Utils\String::areSameWord($ext, $known_ext->label)) {
                return $known_ext->type;
            }
        }
        return Enums\FileExt::NEO_FILE_TYPE_MISC;
    }

    /**
     * Make directory if it is missing
     *
     * @param $path
     * @param int $permissions
     */

    static function makeMissingDir ($dir, $permissions = 0755) {
        $parts = explode('/', $dir);

        /**
         * Remove last part if it is a filename
         */
        if(strpos($parts[count($parts)-1], '.') !== false ) {
            array_pop($parts);
        }

        $dir = '';
        foreach ($parts as $part) {
            $dir = Utils\File::trimLeftSlash($dir .= "/$part");
            if (!is_dir($dir)) {
                mkdir($dir, $permissions, true);
            }
        }
    }
} 