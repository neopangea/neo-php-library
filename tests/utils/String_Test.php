<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/25/14
 * Time: 7:34 AM
 */

class String_Test extends PHPUnit_Framework_TestCase {

    public function testFirstLetterCorrect() {
        $result = Neo\Lib\Utils\String::getFirstLetter('test');
        $this->assertEquals('t', $result);
    }

    public function testFirstLetterBlank() {
        $result = Neo\Lib\Utils\String::getFirstLetter('');
        $this->assertEquals('', $result);
    }

    public function testFirstLetterNull() {
        $result = Neo\Lib\Utils\String::getFirstLetter(null);
        $this->assertEquals('', $result);
    }

    public function testStringSame() {
        $result = Neo\Lib\Utils\String::areSameWord(' hello', 'HELLO ');
        $this->assertEquals(true, $result);
    }

    public function testStringNull() {
        $result = Neo\Lib\Utils\String::isNullOrEmpty(null);
        $this->assertEquals(true, $result);
    }

    public function testStringEmpty1() {
        $result = Neo\Lib\Utils\String::isNullOrEmpty("");
        $this->assertEquals(true, $result);
    }

    public function testStringEmpty2() {
        $result = Neo\Lib\Utils\String::isNullOrEmpty("  ");
        $this->assertEquals(true, $result);
    }

    public function testStringEmpty3() {
        $result = Neo\Lib\Utils\String::isNullOrEmpty("hello");
        $this->assertEquals(false, $result);
    }

    public function testStringEmpty4() {
        $result = Neo\Lib\Utils\String::isNullOrEmpty(2);
        $this->assertEquals(false, $result);
    }
} 