<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/25/14
 * Time: 7:34 AM
 */

use Neo\Lib\Utils as Utils;
use Neo\Lib\Enums as Enums;

class File_Test extends PHPUnit_Framework_TestCase {

    public function testFileType() {

        $result = Utils\File::getFileType('test.gif');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_IMAGE, $result);

        $result = Utils\File::getFileType('test.jpeg');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_IMAGE, $result);

        $result = Utils\File::getFileType('test.jpg');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_IMAGE, $result);

        $result = Utils\File::getFileType('test.png');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_IMAGE, $result);

        $result = Utils\File::getFileType('test.mp3');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_AUDIO, $result);

        $result = Utils\File::getFileType('test.pdf');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_DOCUMENT, $result);

        $result = Utils\File::getFileType('test.blah');
        $this->assertEquals(Enums\FileExt::NEO_FILE_TYPE_MISC, $result);
    }

    public function testGetFilename () {
        $result = Utils\File::getFilename('some/dir/test.gif');
        $this->assertEquals('test.gif', $result);
    }

    public function testAddPostfix () {
        $result = Utils\File::addPostfix('test.gif', '_orig');
        $this->assertEquals('test_orig.gif', $result);

        $result = Utils\File::addPostfix('/some/dir/test.gif', '_orig');
        $this->assertEquals('/some/dir/test_orig.gif', $result);
    }

    public function testRemoveFileExt () {
        $result = Utils\File::removeFileExt('test.gif');
        $this->assertEquals('test', $result);

        $result = Utils\File::removeFileExt('/some/dir/test.gif');
        $this->assertEquals('/some/dir/test', $result);
    }

    public function testGetFileExt () {
        $result = Utils\File::getFileExt('test.gif');
        $this->assertEquals('gif', $result);

        $result = Utils\File::getFileExt('/some/dir/test.gif');
        $this->assertEquals('gif', $result);
    }

    public function testTrimRightSlash () {
        $result = Utils\File::trimRightSlash('test.gif');
        $this->assertEquals('test.gif', $result);

        $result = Utils\File::trimRightSlash('test.gif/');
        $this->assertEquals('test.gif', $result);

        $result = Utils\File::trimRightSlash('test.gif/ ');
        $this->assertEquals('test.gif', $result);

        $result = Utils\File::trimRightSlash('test.gif\\');
        $this->assertEquals('test.gif', $result);

        $result = Utils\File::trimRightSlash('test.gif\ ');
        $this->assertEquals('test.gif', $result);

    }

    public function testTrimLeftSlash_01 () {
        $result = Utils\File::trimLeftSlash('test.gif/');
        $this->assertEquals('test.gif/', $result);
    }

    public function testTrimLeftSlash_02 () {
        $result = Utils\File::trimLeftSlash('/test.gif/');
        $this->assertEquals('test.gif/', $result);
    }

    public function testTrimLeftSlash_03 () {
        $result = Utils\File::trimLeftSlash('\test.gif ');
        $this->assertEquals('test.gif', $result);
    }

    public function testTrimLeftSlash_04 () {
        $result = Utils\File::trimLeftSlash('\\test.gif');
        $this->assertEquals('test.gif', $result);
    }

    public function testTrimLeftSlash_05 () {
        $result = Utils\File::trimLeftSlash(' \test.gif');
        $this->assertEquals('test.gif', $result);
    }
} 